let a = document.getElementsByClassName("feature");
let b = document.querySelectorAll(".feature");
console.log(a, b);

for (let element of a) {
  element.style.textAlign = "center";
}

let headingElement = document.getElementsByTagName("h2");
for (let h2Element of headingElement) {
  h2Element.textContent = "Awesome feature";
}

const featureTitleElements = document.querySelectorAll(".feature-title");

for (let featureTitleElement of featureTitleElements) {
  featureTitleElement.textContent += "!";
}
